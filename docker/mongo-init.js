// // Users

// db.createUser({
//     user: 'mongo',
//     pwd: 'mongo',
//     roles: [
//         {
//             role: 'readWrite',
//             db: 'isc-dashboard',
//         },
//     ],
// });

// let date = new Date();
// // Collections

// const portfolioISCMarocOID = ObjectId();

// const projects = [
//     {
//         _id: ObjectId(),
//         name: 'Projectfirst',
//         generalInformation: {
//             client: 'ISC Maroc',
//             signingAgency: 'ISC Maroc',
//             workingAgency: 'ISC France',
//             appCode: 'OP09',
//             bwProject: 'default',
//             activityType: 'activity',
//             lifeCycle: 'Continue',
//             responsible: 'khalid dada',
//         },
//         budgetPlaning: {
//             initialBudget: 200000,
//             initialMargin: 34,
//             totalWorkload: 20,
//             devWorkload: 200,
//             contractStartDate: '20/11/2019',
//             contractEndDate: '04/03/2020',
//         },
//         goalsVision: {
//             vision: 'Evolution',
//             goals: 'Achievement',
//             SLAS: 'Default',
//             penalties: 'None',
//         },
//         technologies: {
//             managementTools: 'Jira',
//             technologies: 'React,GraphQL',
//         },
//     },
//     {
//         _id: ObjectId(),
//         name: 'Project 2',
//         generalInformation: {
//             client: 'ISC Maroc',
//             signingAgency: 'ISC Maroc',
//             workingAgency: 'ISC France',
//             appCode: 'OP09',
//             bwProject: 'default',
//             activityType: 'activity',
//             lifeCycle: 'Continue',
//             responsible: 'khalid dada',
//         },
//         budgetPlaning: {
//             initialBudget: 200000,
//             initialMargin: 34,
//             totalWorkload: 20,
//             devWorkload: 200,
//             contractStartDate: '20/11/2019',
//             contractEndDate: '04/03/2020',
//         },
//         goalsVision: {
//             vision: 'Evolution',
//             goals: 'Achievement',
//             SLAS: 'Default',
//             penalties: 'None',
//         },
//         technologies: {
//             managementTools: 'Jira',
//             technologies: 'React,GraphQL',
//         },
//     },
//     {
//         _id: ObjectId(),
//         name: 'Project 3',
//         generalInformation: {
//             client: 'ISC Maroc',
//             signingAgency: 'ISC Maroc',
//             workingAgency: 'ISC France',
//             appCode: 'OP09',
//             bwProject: 'default',
//             activityType: 'activity',
//             lifeCycle: 'Continue',
//             responsible: 'khalid dada',
//         },
//         budgetPlaning: {
//             initialBudget: 200000,
//             initialMargin: 34,
//             totalWorkload: 20,
//             devWorkload: 200,
//             contractStartDate: '20/11/2019',
//             contractEndDate: '04/03/2020',
//         },
//     },
//     {
//         _id: ObjectId(),
//         name: 'Project 4',
//         generalInformation: {
//             client: 'ISC Maroc',
//             signingAgency: 'ISC Maroc',
//             workingAgency: 'ISC France',
//             appCode: 'OP09',
//             bwProject: 'default',
//             activityType: 'activity',
//             lifeCycle: 'Continue',
//             responsible: 'khalid dada',
//         },
//         budgetPlaning: {
//             initialBudget: 200000,
//             initialMargin: 34,
//             totalWorkload: 20,
//             devWorkload: 200,
//             contractStartDate: '20/11/2019',
//             contractEndDate: '04/03/2020',
//         },
//     },
//     {
//         _id: ObjectId(),
//         name: 'Project 5',
//         generalInformation: {
//             client: 'ISC Maroc',
//             signingAgency: 'ISC Maroc',
//             workingAgency: 'ISC France',
//             appCode: 'OP09',
//             bwProject: 'default',
//             activityType: 'activity',
//             lifeCycle: 'Continue',
//             responsible: 'khalid dada',
//         },
//         budgetPlaning: {
//             initialBudget: 200000,
//             initialMargin: 34,
//             totalWorkload: 20,
//             devWorkload: 200,
//             contractStartDate: '20/11/2019',
//             contractEndDate: '04/03/2020',
//         },
//     },
//     {
//         _id: ObjectId(),
//         name: 'Project 6',
//         generalInformation: {
//             client: 'ISC Maroc',
//             signingAgency: 'ISC Maroc',
//             workingAgency: 'ISC France',
//             appCode: 'OP09',
//             bwProject: 'default',
//             activityType: 'activity',
//             lifeCycle: 'Continue',
//             responsible: 'khalid dada',
//         },
//         budgetPlaning: {
//             initialBudget: 200000,
//             initialMargin: 34,
//             totalWorkload: 20,
//             devWorkload: 200,
//             contractStartDate: '20/11/2019',
//             contractEndDate: '04/03/2020',
//         },
//     },
// ];

// db.portfolios.insertMany([
//     {
//         _id: portfolioISCMarocOID,
//         name: 'ISC Maroc',
//         generalInformation: {
//             client: 'ISC Maroc',
//             signingAgency: 'ISC Maroc',
//             workingAgency: 'ISC France',
//             responsible: 'khalid dada',
//         },
//         goalsVision: {
//             vision: 'Evolution',
//             goals: 'Achievement',
//             SLAS: 'Default',
//             penalties: 'None',
//         },
//         budgetPlaning: {
//             initialBudget: 200000,
//             initialMargin: 34,
//             totalWorkload: 20,
//             devWorkload: 200,
//             contractStartDate: '20/11/2019',
//             contractEndDate: '04/03/2020',
//         },
//         technologies: {
//             managementTools: 'Jira',
//             technologies: 'React,GraphQL',
//         },
//     },
//     {
//         _id: ObjectId(),
//         name: 'ISC France',
//         generalInformation: {
//             client: 'ISC Maroc',
//             signingAgency: 'ISC Maroc',
//             workingAgency: 'ISC France',
//             responsible: 'khalid dada',
//         },
//         budgetPlaning: {
//             initialBudget: 200000,
//             initialMargin: 34,
//             totalWorkload: 20,
//             devWorkload: 200,
//             contractStartDate: '20/11/2019',
//             contractEndDate: '04/03/2020',
//         },
//         goalsVision: {
//             vision: 'Evolution',
//             goals: 'Achievement',
//             SLAS: 'Default',
//             penalties: 'None',
//         },
//         technologies: {
//             managementTools: 'Jira',
//             technologies: 'React,GraphQL',
//         },
//         projects: [
//             projects[0]._id.valueOf(),
//             projects[1]._id.valueOf(),
//             projects[2]._id.valueOf(),
//         ],
//     },
//     {
//         _id: ObjectId(),
//         name: 'Portfolio 1',
//         budgetPlaning: {
//             initialBudget: 200000,
//             initialMargin: 34,
//             totalWorkload: 20,
//             devWorkload: 200,
//             contractStartDate: '20/11/2019',
//             contractEndDate: '04/03/2020',
//         },
//         programs: [
//             {
//                 _id: ObjectId(),
//                 name: 'Program 1',
//                 generalInformation: {
//                     client: 'ISC Maroc',
//                     signingAgency: 'ISC Maroc',
//                     workingAgency: 'ISC France',
//                     responsible: 'khalid dada',
//                 },
//                 goalsVision: {
//                     vision: 'Evolution',
//                     goals: 'Achievement',
//                     SLAS: 'Default',
//                     penalties: 'None',
//                 },
//                 technologies: {
//                     managementTools: 'Jira',
//                     technologies: 'React,GraphQL',
//                 },
//                 projects: [
//                     projects[3]._id.valueOf(),
//                     projects[4]._id.valueOf(),
//                     projects[5]._id.valueOf(),
//                 ],
//             },
//             {
//                 _id: ObjectId(),
//                 name: 'Program 2',
//                 generalInformation: {
//                     client: 'ISC Maroc',
//                     signingAgency: 'ISC Maroc',
//                     workingAgency: 'ISC France',
//                     responsible: 'khalid dada',
//                 },
//                 goalsVision: {
//                     vision: 'Evolution',
//                     goals: 'Achievement',
//                     SLAS: 'Default',
//                     penalties: 'None',
//                 },
//                 budgetPlaning: {
//                     initialBudget: 200000,
//                     initialMargin: 34,
//                     totalWorkload: 20,
//                     devWorkload: 200,
//                     contractStartDate: '20/11/2019',
//                     contractEndDate: '04/03/2020',
//                 },
//                 technologies: {
//                     managementTools: 'Jira',
//                     technologies: 'React,GraphQL',
//                 },
//             },
//             {
//                 _id: ObjectId(),
//                 name: 'Program 3',
//                 generalInformation: {
//                     client: 'ISC Maroc',
//                     signingAgency: 'ISC Maroc',
//                     workingAgency: 'ISC France',
//                     responsible: 'khalid dada',
//                 },
//                 budgetPlaning: {
//                     initialBudget: 200000,
//                     initialMargin: 34,
//                     totalWorkload: 20,
//                     devWorkload: 200,
//                     contractStartDate: '20/11/2019',
//                     contractEndDate: '04/03/2020',
//                 },
//                 technologies: {
//                     managementTools: 'Jira',
//                     technologies: 'React,GraphQL',
//                 },
//             },
//         ],
//         parent: portfolioISCMarocOID.valueOf(),
//     },
//     {
//         _id: ObjectId(),
//         name: 'Portfolio 2',
//         parent: portfolioISCMarocOID.valueOf(),
//         programs: [
//             {
//                 _id: ObjectId(),
//                 name: 'Program 4',
//                 budgetPlaning: {
//                     initialBudget: 200000,
//                     initialMargin: 34,
//                     totalWorkload: 20,
//                     devWorkload: 200,
//                     contractStartDate: '20/11/2019',
//                     contractEndDate: '04/03/2020',
//                 },
//             },
//             {
//                 _id: ObjectId(),
//                 name: 'Program 5',
//                 budgetPlaning: {
//                     initialBudget: 200000,
//                     initialMargin: 34,
//                     totalWorkload: 20,
//                     devWorkload: 200,
//                     contractStartDate: '20/11/2019',
//                     contractEndDate: '04/03/2020',
//                 },
//             },
//             {
//                 _id: ObjectId(),
//                 name: 'Program 6',
//                 budgetPlaning: {
//                     initialBudget: 200000,
//                     initialMargin: 34,
//                     totalWorkload: 20,
//                     devWorkload: 200,
//                     contractStartDate: '20/11/2019',
//                     contractEndDate: '04/03/2020',
//                 },
//             },
//         ],
//     },
//     {
//         _id: ObjectId(),
//         name: 'Portfolio 3',
//         budgetPlaning: {
//             initialBudget: 200000,
//             initialMargin: 34,
//             totalWorkload: 20,
//             devWorkload: 200,
//             contractStartDate: '20/11/2019',
//             contractEndDate: '04/03/2020',
//         },
//         parent: portfolioISCMarocOID.valueOf(),
//     },
// ]);

// db.projects.insertMany(projects);

// db.agencies.insertMany([
//     {
//         _id: ObjectId(),
//         name: 'agency1',
//     },
//     {
//         _id: ObjectId(),
//         name: 'agency2',
//     },
//     {
//         _id: ObjectId(),
//         name: 'agency3',
//     },
//     {
//         _id: ObjectId(),
//         name: 'agency4',
//     },
//     {
//         _id: ObjectId(),
//         name: 'agency5',
//     },
// ]);

// db.listvalues.insertMany([
//     {
//         _id: ObjectId(),
//         name: 'Intern - Idle time',
//         listValueType: 'bwProject',
//     },
//     {
//         _id: ObjectId(),
//         name: 'Intern - Mngt / Meetings',
//         listValueType: 'bwProject',
//     },
//     {
//         _id: ObjectId(),
//         name: 'Customer - Fixed Price',
//         listValueType: 'bwProject',
//     },
//     {
//         _id: ObjectId(),
//         name: 'Project Build',
//         listValueType: 'activityType',
//     },
//     {
//         _id: ObjectId(),
//         name: 'Release Build',
//         listValueType: 'activityType',
//     },
//     {
//         _id: ObjectId(),
//         name: 'Adaptive (Agile)',
//         listValueType: 'lifeCycle',
//     },
//     {
//         _id: ObjectId(),
//         name: 'Iterative & Incremental',
//         listValueType: 'lifeCycle',
//     },
//     {
//         _id: ObjectId(),
//         name: 'Jira SQLi',
//         listValueType: 'managementTools',
//     },
//     {
//         _id: ObjectId(),
//         name: 'Jira Client',
//         listValueType: 'managementTools',
//     },
//     {
//         _id: ObjectId(),
//         name: '.NET',
//         listValueType: 'technologies',
//     },
//     {
//         _id: ObjectId(),
//         name: 'PHP/FRONT/Mobile',
//         listValueType: 'technologies',
//     },
// ]);
