import { Product } from "../../../ApolloConfig/models";
import { getModel } from "../../Switchdb/model.find";


export const DeleteProduct = async (id, context) => {
    try {
        // get last facture in this years
        const ProductModel = await getModel(
            [Product],
            context.companyName
        );

        let doc = await ProductModel.deleteOne({ _id: id });

        return doc.deletedCount > 0 ? true : false;
    } catch (error) {
        throw new Error(error);
    }
};

export const DeleteProducts = async (ids, context) => {
    try {
        // get last facture in this years
        const ProductModel = await getModel(
            [Product],
            context.companyName
        );

        console.log(ids);

        let doc = await ProductModel.deleteMany({
            _id: { $in: ids },
        });

        return doc.deletedCount > 0 ? true : false;
    } catch (error) {
        throw new Error(error);
    }
};