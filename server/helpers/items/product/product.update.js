import { Product } from "../../../ApolloConfig/models";
import { getModel } from "../../Switchdb/model.find";


export const UpdateProduct = async (
    {
    nom,
    reference,
    info,
    prix_ht,
    prix_achat    
    },
    id,
    context
) => {
    try {
        const ProductModel = await getModel(
            [Product],
            context.companyName
        );

        const UpdateFacture = await ProductModel.findByIdAndUpdate({ _id: id },
            {
                nom,
                reference,
                info,
                prix_ht,
                prix_achat
            }
        );

        const doc = await ProductModel.findOne({ _id: id });

        return doc;
    } catch (error) {
        throw new Error(error);
    }
};