import {
    Product,
} from '../../../ApolloConfig/models';
import { getModel } from '../../Switchdb/model.find';

export const AddProduct = async (
    {
        nom,
        reference,
        info,
        prix_ht,
        prix_achat
    },
    context
) => {
    try {
        const ProductModel = await getModel(
            [Product],
            context.companyName
        );

        // create product
        const newProduct = await ProductModel.create({
            nom,
            reference,
            info,
            prix_ht,
            prix_achat
        });

        return newProduct;
    } catch (error) {
        throw new Error(error);
    }
};