import mongoose from "mongoose";
import { Client, Devis, Product } from "../../../ApolloConfig/models";
import { getModel } from "../../Switchdb/model.find";

export const getDevis = async (_id, context) => {
    try {
        const DevisModel = await getModel(
            [Devis, Product, Client],
            context.companyName
        );    
    
        return await DevisModel.findById(_id).populate({path:"products.product", model:"product"}).populate({path:"client.ref", model:"client"})
    } catch (error) {
        throw new Error(error);
    }
}