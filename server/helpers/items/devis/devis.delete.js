import { ApolloError } from 'apollo-server';
import {
    Employee,
    Devis,
} from '../../../ApolloConfig/models';
import { getModel } from '../../Switchdb/model.find';

export const DeleteDevis = async (id, context) => {
    try {
        // get last Devis in this years
        const DevisModel = await getModel(
            [Devis],
            context.companyName
        );

        let doc = await DevisModel.deleteOne({ _id: id });

        return doc.deletedCount > 0 ? true : false;
    } catch (error) {
        throw new Error(error);
    }
};

export const DeleteAllDevis = async (ids, context) => {
    try {
        // get last Devis in this years
        const DevisModel = await getModel(
            [Devis],
            context.companyName
        );

        console.log(ids);

        let doc = await DevisModel.deleteMany({
            _id: { $in: ids },
        });

        return doc.deletedCount > 0 ? true : false;
    } catch (error) {
        throw new Error(error);
    }
};
