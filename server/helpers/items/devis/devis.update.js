import { ApolloError } from 'apollo-server';
import {
    Employee,
    Devis,
} from '../../../ApolloConfig/models';
import { getModel } from '../../Switchdb/model.find';
import { getDevis } from './devis.get';

export const UpdateDevis = async (
    {
        client,
        products,
        status,
        tva,
        conditions,
        notes,
        date_validation,
        date_emission,
    },
    id,
    context
) => {
    try {
        // get last Devis in this years
        const DevisModel = await getModel(
            [Devis],
            context.companyName
        );

        const parseDateValidation = date_validation.split(
            '/'
        );
        // month is 0-based, that's why we need dataParts[1] - 1
        const dateV = new Date(
            +parseDateValidation[2],
            parseDateValidation[1] - 1,
            +parseDateValidation[0]
        );

        const parseDateEmission = date_emission.split('/');
        // month is 0-based, that's why we need dataParts[1] - 1
        const dateE = new Date(
            +parseDateEmission[2],
            parseDateEmission[1] - 1,
            +parseDateEmission[0]
        );

        const UpdateDevis = await DevisModel.findByIdAndUpdate(
            { _id: id },
            {
                client: client,
                products: products,
                status: status,
                tva: tva,
                conditions: conditions,
                notes: notes,
                date_validation: new Date(dateV),
                date_emission: new Date(dateE),
            }
        );

        const doc = getDevis(UpdateDevis._id, context);

        return doc;
    } catch (error) {
        throw new Error(error);
    }
};
