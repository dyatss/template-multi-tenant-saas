import { ApolloError } from 'apollo-server';
import {
    Employee,
    Facture,
} from '../../../ApolloConfig/models';
import { getModel } from '../../Switchdb/model.find';

export const DeleteFacture = async (id, context) => {
    try {
        // get last facture in this years
        const FactureModel = await getModel(
            [Facture],
            context.companyName
        );

        let doc = await FactureModel.deleteOne({ _id: id });

        return doc.deletedCount > 0 ? true : false;
    } catch (error) {
        throw new Error(error);
    }
};

export const DeleteFactures = async (ids, context) => {
    try {
        // get last facture in this years
        const FactureModel = await getModel(
            [Facture],
            context.companyName
        );

        console.log(ids);

        let doc = await FactureModel.deleteMany({
            _id: { $in: ids },
        });

        return doc.deletedCount > 0 ? true : false;
    } catch (error) {
        throw new Error(error);
    }
};
