import { ApolloError } from 'apollo-server';
import mongoose from 'mongoose';
import {
    Employee,
    Facture,
} from '../../../ApolloConfig/models';
import { getModel } from '../../Switchdb/model.find';
import { getFactures } from './facture.get';

export const AddFacture = async (
    {
        devis,
        client,
        products,
        status,
        tva,
        conditions,
        notes,
        date_validation,
        date_emission,
    },
    context
) => {
    try {
        // find user

        const UserModel = await getModel(
            [Employee],
            context.companyName
        );

        const user = await UserModel.findById(
            context.user_id
        );

        // get last facture in this years
        const FactureModel = await getModel(
            [Facture],
            context.companyName
        );

        const yyyy = new Date().getFullYear();
            
        const lastFacture = await FactureModel.find({
            createdAt: {
                $gte: new Date(yyyy, 0, 0),
                $lt: new Date(yyyy + 1, 1, 1),
            },
        })
            .sort({ createdAt: -1 })
            .limit(1)
            .select({ reference: 1, _id: 0 });

        // console.log("----------------------------------------")
        // console.log("last facture")
        // console.log("----------------------------------------")
        // console.log(await lastFacture)
        // console.log("----------------------------------------")

        let ref = 'F-' + yyyy.toString().substr(-2) + '-';

        if (lastFacture.length === 0) {
            console.log('is empty');
            ref = ref + '001';
        } else {
            const lastnum = lastFacture[0].reference.split(
                '-'
            )[2];
            if (parseInt(lastnum) >= 999) {
                ref =
                    ref + (parseInt(lastnum) + 1)
            } else {
                ref =
                    ref +
                    ('00' + (parseInt(lastnum) + 1)).slice(-3);
            }

            console.log(ref);
        }

        const parseDateValidation = date_validation.split(
            '/'
        );
        // month is 0-based, that's why we need dataParts[1] - 1
        const dateV = new Date(
            +parseDateValidation[2],
            parseDateValidation[1] - 1,
            +parseDateValidation[0]
        );

        const parseDateEmission = date_emission.split('/');
        // month is 0-based, that's why we need dataParts[1] - 1
        const dateE = new Date(
            +parseDateEmission[2],
            parseDateEmission[1] - 1,
            +parseDateEmission[0]
        );

        // let newproduct = [... products]

        // newproduct.map(product => {
        //     product.id = new mongoose.Types.ObjectId(product.id);
        // })

        // create facture
        const newFacture = await FactureModel.create({
            reference: ref,
            devis: devis,
            client: client,
            products: products,
            status: status,
            tva: tva,
            conditions: conditions,
            notes: notes,
            date_validation: new Date(dateV),
            date_emission: new Date(dateE),
            author: user,
        })
        return getFactures(newFacture._id, context);
    } catch (error) {
        throw new Error(error);
    }
};
