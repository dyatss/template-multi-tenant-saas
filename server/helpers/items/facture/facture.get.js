import mongoose from "mongoose";
import { Client, Facture, Product, Devis } from "../../../ApolloConfig/models";
import { getModel } from "../../Switchdb/model.find";

export const getFactures = async (_id, context) => {
    try {
        const FactureModel = await getModel(
            [Facture, Product, Client, Devis],
            context.companyName
        );    
    
        return await FactureModel.findById(_id).populate({path:"products.product", model:"product"}).populate({path:"client.ref", model:"client"}).populate({path:"devis", model:"devis"})
    } catch (error) {
        throw new Error(error);
    }
}