import { ApolloError } from "apollo-server"
import { Employee } from "../../../ApolloConfig/models"
import { getModel } from "../../Switchdb/model.find"


var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;

export const findUserById = async ({id, locataire}) => {
    const EmployeeModel = await getModel((Employee), locataire)
    // see if an user exists with email
    return await EmployeeModel.findById(id)
}

export const login = async ({LoginInput:{email, password}, locataire}) => {

    const EmployeeModel = await getModel([Employee], locataire)
    // see if user exists with the email
    let user = await EmployeeModel.findOne({ email })

    if(!user){
        throw new ApolloError('This email not exist ' + email, 'USER_NOT_EXISTS');
    }
    // comapre password

    if(user && (await bcrypt.compare(password, user.password))){
        const token = jwt.sign(
            {
                name: user.name,
                companyName: user.companyName,
                user_id: user._id, 
                email: user.email, 
                username: user.username,
                password : user.password,
              },
            process.env.APP_SECRET,
            {
              expiresIn: process.env.TOKEN_EXPIRES_IN,
            }
          )

        const newUserToken = await EmployeeModel.updateOne({email}, {
            token: token
        })

        const doc = await EmployeeModel.findOne({email});
    
        return{
        id: doc.id,
        ...doc._doc
        }
    } else {
        throw new ApolloError('Incorrect password', 'INCORECT_PASSWORD');
    }
}