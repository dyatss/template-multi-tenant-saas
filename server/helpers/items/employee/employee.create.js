import { ApolloError } from "apollo-server"
import { Employee } from "../../../ApolloConfig/models"
import { getModel } from "../../Switchdb/model.find"

var jwt = require('jsonwebtoken');

export const registerUser = async (
    {RegisterInput, locataire}
) => {

    
    const EmployeeModel = await getModel([Employee], locataire)
    // see if an user exists with email
    const oldUser = await EmployeeModel.findOne({email:RegisterInput.email})

    if(oldUser){
        throw new ApolloError('A employee is already registered with the email ' + RegisterInput.email, 'USER_ALREADY_EXISTS');
    }

    // build out mongoose model (Employee)
    const newUser = new EmployeeModel({
        companyName: locataire,
        username: RegisterInput.username,
        email: RegisterInput.email.toLowerCase(),
        password: RegisterInput.password,
        role: "Admin"
    })

    

    const res = await newUser.save()

    return {
        id: res.id,
        ...res._doc
    }
}