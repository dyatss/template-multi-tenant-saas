import {
    Client,
} from '../../../ApolloConfig/models';
import { getModel } from '../../Switchdb/model.find';

export const AddClient = async (
    input,
    context
) => {
    try {
        const clientModel = await getModel([Client], context.companyName);

        

        const NewClient = clientModel.create(input);

        return NewClient
    } catch (error) {
        throw new Error(error);
    }
};