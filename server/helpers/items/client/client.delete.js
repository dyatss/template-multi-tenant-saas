import { Client } from "../../../ApolloConfig/models";
import { getModel } from "../../Switchdb/model.find";


export const DeleteClient = async (id, context) => {
    try {
        // get last facture in this years
        const ClientModel = await getModel(
            [Client],
            context.companyName
        );

        let doc = await ClientModel.deleteOne({ _id: id });

        return doc.deletedCount > 0 ? true : false;
    } catch (error) {
        throw new Error(error);
    }
};

export const DeleteClients = async (ids, context) => {
    try {
        // get last facture in this years
        const ClientModel = await getModel(
            [Client],
            context.companyName
        );

        console.log(ids);

        let doc = await ClientModel.deleteMany({
            _id: { $in: ids },
        });

        return doc.deletedCount > 0 ? true : false;
    } catch (error) {
        throw new Error(error);
    }
};