import { Client } from "../../../ApolloConfig/models";
import { getModel } from "../../Switchdb/model.find";


export const UpdateClient = async (
    input,
    id,
    context
) => {
    try {
        const ClientModel = await getModel(
            [Client],
            context.companyName
        );

        const UpdateClient = await ClientModel.findByIdAndUpdate({ _id: id },
            input
        );

        const doc = await ClientModel.findOne({ _id: id });

        return doc;
    } catch (error) {
        throw new Error(error);
    }
};