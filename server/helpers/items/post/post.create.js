import { Employee, Post } from "../../../ApolloConfig/models"
import { getModel } from "../../Switchdb/model.find"

export const createPost = async (
    {title, content}, context
) => {
    try {
        

        // find user 

        const UserModel = await getModel([Employee], context.companyName)

        const user = await UserModel.findById(context.user_id)

        // create post
        const PostModel = await getModel([Post], context.companyName)

        const newPost = await PostModel.create({
        title: title,
        content: content,
        author: user
        })
        return newPost
    } catch (error) {
        throw new Error(error)
    }
}