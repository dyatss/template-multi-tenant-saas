import { ApolloError } from "apollo-server"
import { Tenant } from "../../../ApolloConfig/models"
import { Employee } from "../../../ApolloConfig/models"
import { rolesEnum } from "../../../ApolloConfig/TypeDefs/types";
import { getModel } from "../../Switchdb/model.find"

var jwt = require('jsonwebtoken');

export const tenantCreate = async (tenantInput) => {

    try {
        const TenantModel = await getModel([Tenant], 'AppTenants')

        const tenant = new TenantModel(tenantInput)

        const employeeModel = await getModel([Employee], tenantInput.companyName)

        const newUserInDB = new employeeModel({
            companyName: tenantInput.companyName,
            name: tenantInput.name,
            email: tenantInput.email.toLowerCase(),
            password: tenantInput.password,
            username: tenantInput.name,
            role: "Super_Admin"
        })
    
        
    
        const res = await tenant.save()
        const newUser = await newUserInDB.save()

        return res


    } catch (error) {
        return error
    }

    


}