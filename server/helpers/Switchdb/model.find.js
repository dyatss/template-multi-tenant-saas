import * as db from '../../manager/manager.js'

export const getModel = async (schemaItem, locataire) => {
    
    const schema = schemaItem
    
    const tenanDb = await db.switchDB(locataire, schema)
    
    const [firstKey] = schemaItem[0].keys();

    const model = await db.getDBModel(tenanDb, firstKey)
    return model
};