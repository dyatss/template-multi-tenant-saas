import mongoose from 'mongoose'

const tenantSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  companyName: {
    type: String,
    unique: true,
    required: true,
  },
})

export const Tenant = new Map([['tenant', tenantSchema]])