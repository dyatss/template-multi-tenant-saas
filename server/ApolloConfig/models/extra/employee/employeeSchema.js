import pkg from 'mongoose';
var jwt = require('jsonwebtoken');

const { Schema, model } = pkg;
var mongoose = require('mongoose'),
    bcrypt = require('bcrypt'),
    SALT_WORK_FACTOR = 10;

var employeeSchema = new Schema(
    {
        name: {
            type: String,
        },
        companyName: {
            type: String,
        },
        email: {
            type: String,
            required: true,
            unique: true,
        },
        username: {
            type: String,
            required: true,
            unique: true,
        },
        password: {
            type: String,
            require: true,
        },
        token: {
            type: String,
        },
        role: {
            type: Array,
            default: undefined,
        },
    },
    {
        timestamps: true,
        versionKey: false,
    }
);

// @ts-ignore
employeeSchema.pre('save', async function (next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    // @ts-ignore
    if (!user.isModified('password')) return next();

    const hash = await bcrypt.hash(
        user.password,
        SALT_WORK_FACTOR
    );
    user.password = hash;
    const token = jwt.sign(
        {
            name: user.name,
            companyName: user.companyName,
            user_id: user._id,
            email: user.email,
            username: user.username,
            password: hash,
        },
        process.env.APP_SECRET,
        {
            expiresIn: process.env.TOKEN_EXPIRES_IN,
        }
    );

    user.token = token;
    next();
    // generate a salt
});

// employeeSchema.methods.comparePassword = function(candidatePassword, cb) {
//   var user = this;
//   // @ts-ignore
//   bcrypt.compare(candidatePassword, user.password, function(err, isMatch) {
//       if (err) return cb(err);
//       cb(null, isMatch);
//   });
// };

export const Employee = new Map([
    ['employee', employeeSchema],
]);
