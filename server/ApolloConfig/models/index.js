export * from './items'
export * from './extra'

import * as items from './items'
import * as extra from './extra'

export const schema = [items.Facture, items.Post, items.Product, extra.Employee, extra.Tenant]