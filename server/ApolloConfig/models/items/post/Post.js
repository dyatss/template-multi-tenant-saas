import pkg from 'mongoose'

const { Schema, model } = pkg

const postSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  content: {
    type: String,
    required: true,
  },
  author: Object
});


// export const Post = model('post', postSchema);

export const Post = new Map([['post', postSchema]])

