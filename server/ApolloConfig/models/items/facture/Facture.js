import pkg from 'mongoose';
import { schema } from '../..';

const { Schema, model } = pkg;

const factureSchema = new Schema(
    {
        reference: {
            type: String,
            required: true,
            unique: true,
        },
        devis: { type: Schema.Types.ObjectId, ref: 'Devis' },
        client: {
            type: Object,
        },
        products: {type: Array},
        status: {
            type: String,
        },
        tva: {
            type: Number,
        },
        conditions: {
            type: String,
        },
        notes: {
            type: String,
        },
        date_validation: {
            type: Date,
            default: Date.now,
        },
        date_emission: {
            type: Date,
            default: Date.now,
        },
        author: {
            type: String,
        },
    },
    {
        timestamps: true,
        versionKey: false,
    }
);

export const Facture = new Map([
    ['facture', factureSchema],
]);
