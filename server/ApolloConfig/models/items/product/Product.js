import mongoose from 'mongoose';
import pkg from 'mongoose';

const { Schema, model } = pkg;

const productSchema = new Schema(
    {
        nom: {
            type: String,
        },
        reference: {
            type: String,
            required: true,
            unique: true,
        },
        description: {
            type: String,
        },
        carateristique: {
            type: String,
        },
        categories: {
            type: Array,
        },
        tags: {
            type: Array,
        },
        info: {
            type: Object,
        },
        notes: {
            type: String,
        },
        prix_ht: {
            type: Number,
        },
        prix_achat: {
            type: Number,
        },
    },
    {
        timestamps: true,
        versionKey: false,
    }
);

export const Product = new Map([
    ['product', productSchema],
]);
