import pkg from 'mongoose';

const { Schema, model } = pkg;

const clientSchema = new Schema(
    {
        code_client: {
            type: String,
            required: true,
            unique: true,
        },

        informations: {
            type: Object
        },
        
        coordonnees: {
            type: Object
        }
    },
    {
        timestamps: true,
        versionKey: false,
    }
);

export const Client = new Map([
    ['client', clientSchema],
]);
