import pkg from 'mongoose';

const { Schema, model } = pkg;

const devisSchema = new Schema(
    {
        reference: {
            type: String,
            required: true,
            unique: true,
        },
        client: {
            type: Object,
        },
        products: {type: Array},
        status: {
            type: String,
        },
        tva: {
            type: Number,
        },
        conditions: {
            type: String,
        },
        notes: {
            type: String,
        },
        date_validation: {
            type: Date,
            default: Date.now,
        },
        date_emission: {
            type: Date,
            default: Date.now,
        },
        author: {
            type: String,
        },
    },
    {
        timestamps: true,
        versionKey: false,
    }
);

export const Devis = new Map([
    ['devis', devisSchema],
]);
