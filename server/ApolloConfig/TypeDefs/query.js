import { gql } from 'apollo-server-express';
import {
    postQuery,
    employeeQuery,
    tenantQuery,
    factureQuery,
    productQuery,
    clientQuery,
    devisQuery
} from './types';

export const query = gql`
    type Query {
        # POST
        ${postQuery}
        ${employeeQuery}
        ${tenantQuery}
        ${devisQuery}
        ${factureQuery}
        ${productQuery}
        ${clientQuery}
    }
`;
