import { gql } from 'apollo-server-express';

export const tenantType = gql`
type Tenant {
  name: String,
  email:String,
  password: String,
  companyName: String,
  }

input TenantInput {
  name: String,
  email: String,
  password: String,
  companyName: String,
}
`;
