import { gql } from 'apollo-server-express';

export const employeeType = gql`
type Employee {
    name: String,
    companyName: String,
    email: String,
    username:String,
    password:String,
    token:String,
    role: [String]
  }
  
  input RegisterInput {
    username: String,
    email: String,
    password: String,
    confirmPassword: String
  }

  input LoginInput {
    email: String,
    password: String,
  }
`;
