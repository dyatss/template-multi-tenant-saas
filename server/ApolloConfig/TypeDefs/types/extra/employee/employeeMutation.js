export const employeeMutation = `
"Register a new Employee @return Employee"
registerUser(RegisterInput: RegisterInput, locataire: String): Employee

"Login a Employee @return Employee"
loginUser(LoginInput: LoginInput, locataire: String): Employee
`;