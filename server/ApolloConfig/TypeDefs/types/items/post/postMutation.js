export const postMutation = `
"Create a new post"
addPost(title: String!, content: String!): Post

"Clear all post"
clearAllPost: Boolean
`;