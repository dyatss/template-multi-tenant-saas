export const postQuery = `
    "Get a post by ID"
    post(_id: ID!): Post

    "Get all post"
    posts: [Post]
`;
