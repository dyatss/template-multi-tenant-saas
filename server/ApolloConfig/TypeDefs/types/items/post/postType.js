import { gql } from 'apollo-server-express';

export const postType = gql`
    type Post {
        _id: ID
        title: String
        content: String
        author: Employee
    }

    type Subscription {
        PostCreated(tenant: String): Post
    }

    type Subscription {
        DeleteAllPosts(tenant: String): String
    }
`;
