export const producteMutation = `
    "Create a product @return Product"
    createProduct(ProductInput: ProductInput!): Product

    "Update a Product by ID @return Product"
    updateProduct(ProductInput: ProductInput, id: ID!): Product

    "Delete a Product by ID @return Boolean"
    deleteProduct(id: ID!): Boolean

    "Delete multiple Product by [ID] @return Boolean"
    deleteProducts(ids: [ID!]!): Boolean
`;
