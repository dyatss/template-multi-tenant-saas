import { gql } from 'apollo-server-express';

export const productType = gql`
    type Product {
        _id: ID
        nom: String
        reference: String
        description: String
        carateristique: String
        categories: [String]
        tags:[String]
        info: ProductInfo
        prix_ht: Float
        prix_achat: Float
    }

    input ProductInput {
        nom: String
        reference: String
        description: String
        carateristique: String
        categories: [String]
        tags:[String]
        info: ProductInfoInput
        prix_ht: Float
        prix_achat: Float
    }

    type ProductInfo{
        color: String
        image: String
        taille: String
        stock: Int
    }

    input ProductInfoInput{
        color: String
        image: String
        taille: String
        stock: Int
    }

`;
