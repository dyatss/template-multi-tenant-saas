import { gql } from 'apollo-server-express';

export const factureType = gql`
    type Facture {
        _id: ID
        reference: String
        devis: Devis
        client: ClientInfoItem
        products: [ProductInfoItem]
        status: Status
        tva: Float
        conditions: String
        notes: String
        date_validation: DateTime
        date_emission: DateTime
        author: Employee
        product:[Product]
    }
    
    input FactureInput {
        devis: ID
        client: ClientInfoItemInput
        products: [ProductInfoItemInput]
        status: Status
        tva: Float
        conditions: String
        notes: String
        date_validation: DateTime
        date_emission: DateTime
    }

    scalar DateTime
`;
