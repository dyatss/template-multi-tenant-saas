export const factureMutation = `
    "Create a facture @return Facture"
    createFacture(FactureInput: FactureInput): Facture

    "Update a facture by ID @return Facture"
    updateFacture(FactureInput: FactureInput, id: ID!): Facture

    "Delete a facture by ID @return Boolean"
    deleteFacture(id: ID!): Boolean

    "Delete multiple facture by [ID] @return Boolean"
    deleteFactures(ids: [ID!]!): Boolean

    "Clear all facture @return Boolean"
    clearAllFactre: Boolean
`;
