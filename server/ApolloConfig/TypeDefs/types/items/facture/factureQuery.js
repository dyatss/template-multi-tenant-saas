export const factureQuery = `
    "Get a Facture by ID @return Facture"
    facture(_id: ID!): Facture

    "Get all Facture @return [Facture]"
    factures: [Facture]

    "Get All facture by year @return [Facture]"
    facturesByYear(year: Int): [Facture]
`;
