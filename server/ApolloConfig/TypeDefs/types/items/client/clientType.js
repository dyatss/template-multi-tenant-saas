import { gql } from 'apollo-server-express';

export const clientType = gql`
    type Client {
        _id: ID
        code_client: String
        informations: Information
        coordonnees: Coordonnee
    }
    
    type Information{
        raison_social: String
        ice: String
        identification_fiscal: String
        rc: String
        cnss: String
        secteur_activite: String
        bank: Bank
        patente: String
    }

    type Bank{
        nom: String
        numero_compte: String
    }

    type Coordonnee {
        code_postal: Int
        adresse: Adresse
        email: EmailAddress
        phoneNumber: PhoneNumber

    }

    type Adresse {
        pays: CountryCode
        ville: String
        adresse: String
        postal: PostalCode
    }

    input ClientInput {
        code_client: String
        informations: InformationInput
        coordonnees: CoordonneeInput
    }

    input InformationInput {
        raison_social: String
        ice: String
        identification_fiscal: String
        rc: String
        cnss: String
        secteur_activite: String
        bank: BankInput
        patente: String
    }

    input CoordonneeInput {
        code_postal: Int
        adresse: AdresseInput
        email: EmailAddress
        phoneNumber: PhoneNumber
    }

    input BankInput {
        nom: String
        numero_compte: String
    }

    input AdresseInput {
        pays: CountryCode
        ville: String
        adresse: String
        postal: PostalCode
    }

    
    scalar EmailAddress
    scalar PhoneNumber
    scalar PostalCode
    scalar CountryCode
`;
