export const clientMutation = `
    "Create a Client @return Client"
    createClient(ClientInput: ClientInput): Client

    "Update a Client @return Client"
    updateClient(ClientInput: ClientInput, id: ID!): Client

    "Delete a Client @return Boolean"
    deleteClient(id: ID!): Boolean

    "Delete multiple Client @return Boolean"
    deleteClients(ids: [ID!]!): Boolean
`;
