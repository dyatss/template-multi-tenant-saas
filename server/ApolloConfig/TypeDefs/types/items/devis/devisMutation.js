export const devisMutation = `
    "Create a Devis @return Devis"
    createDevis(DevisInput: DevisInput): Devis

    "Update a Devis by ID @return Devis"
    updateDevis(DevisInput: DevisInput, id: ID!): Devis

    "Delete a Devis by ID @return Boolean"
    deleteDevis(id: ID!): Boolean

    "Delete multiple Devis by [ID] @return Boolean"
    deleteAllDevis(ids: [ID!]!): Boolean

    "Clear all Devis @return Boolean"
    clearAllDevis: Boolean
`;
