import { gql } from 'apollo-server-express';

export const devisType = gql`
    type Devis {
        _id: ID
        reference: String
        client: ClientInfoItem
        products: [ProductInfoItem]
        status: Status
        tva: Float
        conditions: String
        notes: String
        date_validation: DateTime
        date_emission: DateTime
        author: Employee
        product:[Product]
    }
    
    input DevisInput {
        client: ClientInfoItemInput
        products: [ProductInfoItemInput]
        status: Status
        tva: Float
        conditions: String
        notes: String
        date_validation: DateTime
        date_emission: DateTime
    }

    scalar DateTime
`;
