export const devisQuery = `
    "Get a Devis by ID @return Devis"
    devis(_id: ID!): Devis

    "Get all Devis @return [Devis]"
    allDevis: [Devis]

    "Get All Devis by year @return [Devis]"
    devisByYear(year: Int): [Devis]
`;
