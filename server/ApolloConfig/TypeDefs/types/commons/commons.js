import { gql } from 'apollo-server-express';

export const rolesEnum = gql`
    "Role Enumeration"
    enum Role {
        Super_Admin
        Admin
        Access
    }
`;

export const StatusEnum = gql`
    "Status Enumeration"
    enum Status {
        brouillon
        valider
        regler
        partiellement
        annuler
        payer
    }
`;
