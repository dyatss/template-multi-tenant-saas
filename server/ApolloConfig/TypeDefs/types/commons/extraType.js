import { gql } from 'apollo-server-express';

export const ExtraType = gql`
    type ClientInfoItem {
        ref: Client
        ice: String
        raison_social: String
        adresse: Adresse
    }

    type ProductInfoItem {
        product: Product
        prix_ht: Float
        nom: String
        reference: String
        description: String
        amount: Int
    }

    input ClientInfoItemInput {
        ref: ID
        ice: String
        raison_social: String
    }

    input ProductInfoItemInput {
        product: ID
        prix_ht: Float
        description: String
    }

    scalar DateTime
`;