import * as types from './types';
import { query } from './query';
import { mutation } from './mutation';

export const typeDefs = [
    mutation,
    query,
    types.postType,
    types.employeeType,
    types.tenantType,
    types.devisType,
    types.factureType,
    types.productType,
    types.clientType,

    /* ExtraType */
    types.ExtraType,

    /* Enum */
    types.rolesEnum,
    types.StatusEnum,
];
