import { gql } from 'apollo-server-express';

import {
    postMutation,
    employeeMutation,
    tenantMutation,
    factureMutation,
    producteMutation,
    clientMutation,
    devisMutation
} from './types';


export const mutation = gql`
    type Mutation {
        #POST
        ${postMutation}
        ${employeeMutation}
        ${tenantMutation}
        ${devisMutation}
        ${factureMutation}
        ${producteMutation}
        ${clientMutation}
    }
`;