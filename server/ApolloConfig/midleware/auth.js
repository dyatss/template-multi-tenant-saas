const { AuthenticationError } = require('apollo-server');

const jwt = require('jsonwebtoken');

module.exports.context = (context) => {
    const authHeader = context.req.headers.authorization;
    const operationName = context.req.body.operationName;

    if (authHeader) {
        // bearer ..
        const token = authHeader;

        if (token) {
            try {
                const user = jwt.verify(
                    token,
                    process.env.APP_SECRET
                );
                return user;
            } catch (error) {
                throw new AuthenticationError(
                    'Invalid/Expired token'
                );
            }
        }
        throw new Error(
            "Authentication token must be 'Bearer [token]"
        );
    } else {
        if (
            operationName !== 'LoginUser' &&
            operationName !== 'RegisterUser' &&
            operationName !== 'CreateTenant'
        ) {
            console.log("test : ", operationName)
            if (
                operationName === null ||
                operationName === '' ||
                operationName !== 'IntrospectionQuery'
            ) {
                return 'need to be connected';
            }
            throw new Error('Authentication token needed');
        } else {
            return 'need to be connected';
        }
    }
};
