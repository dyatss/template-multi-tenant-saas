import { registerUser } from '../../../helpers/items/employee/employee.create';
import { findUserById, login } from '../../../helpers/items/employee/employee.find';



// Provide resolver functions for the GraphQL schema
export const EmployeeResolver = {
  /**
   * A GraphQL Query for posts that uses a Post model to query MongoDB
   * and get all Post documents.
   */
  Query: {
    employee: async (_, data) => {
      return await findUserById(data)
    }
  },
  /**
   * A GraphQL Mutation that provides functionality for adding post to
   * the posts list and return post after successfully adding to list
   */
  Mutation: {
    registerUser: async (_, RegisterInput) => {
      console.log("is calling")
      return await registerUser(RegisterInput)
    },
    loginUser: async (_, LoginInput) => {
      return await login(LoginInput)
    }
  }
};
