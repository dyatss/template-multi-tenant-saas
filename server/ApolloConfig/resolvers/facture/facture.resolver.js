import { Facture, Product } from '../../models';
import connectDB from '../../../services/mongo.connect.js';
import { getModel } from '../../../helpers/Switchdb/model.find.js';
import { createPost } from '../../../helpers/items/post/post.create';
import { ApolloError } from 'apollo-server';
import { ObjectId, ObjectID } from 'bson';
import { context } from '../../midleware/auth';
import { AddFacture } from '../../../helpers/items/facture/facture.create';
import { UpdateFacture } from '../../../helpers/items/facture/facture.update';
import {
    DeleteFacture,
    DeleteFactures,
} from '../../../helpers/items/facture/facture.delete';
import mongoose from 'mongoose';
import { getFactures } from '../../../helpers/items/facture/facture.get';
const jwt = require('jsonwebtoken');

// Provide resolver functions for the GraphQL schema
export const factureResolver = {
    /**
     * A GraphQL Query for posts that uses a Post model to query MongoDB
     * and get all Post documents.
     */
    Query: {
        facture: async (parent, {_id}, context) => {
            const facture = getFactures(
                _id,
                context
            )
            return facture
        },

        factures: async (parent,_, context) => {
            const FactureModel = await getModel(
                [Facture],
                context.companyName
            );
            return FactureModel.find({});
        },

        facturesByYear: async (parent, {year} , context) => {
            const FactureModel = await getModel(
                [Facture],
                context.companyName
            );
            return FactureModel.find({
                date_validation: {
                    $gte: new Date(year, 0, 0),
                    $lt: new Date(year + 1, 1, 1),
                },
            });
        },
    },
    /**
     * A GraphQL Mutation that provides functionality for adding post to
     * the posts list and return post after successfully adding to list
     */
    Mutation: {
        createFacture: async (
            parent,
            FactureInput,
            context
        ) => {
            const FactureCreate = await AddFacture(
                FactureInput.FactureInput,
                context
            );
            return FactureCreate;
        },

        updateFacture: async (
            parent,
            FactureInput,
            context
        ) => {
            const FactureCreate = await UpdateFacture(
                FactureInput.FactureInput,
                FactureInput.id,
                context
            );
            return FactureCreate;
        },

        deleteFacture: async (parent, { id }, context) => {
            const FactureCreate = await DeleteFacture(
                id,
                context
            );
            return FactureCreate;
        },

        deleteFactures: async (
            parent,
            { ids },
            context
        ) => {
            const FactureCreate = await DeleteFactures(
                ids,
                context
            );
            return FactureCreate;
        },

        clearAllFactre: async (parent, _, context) => {
            // await context.auth
            const FactureModel = await getModel(
                [Facture],
                context.companyName
            );
            FactureModel.deleteMany({}, function (err) {
                console.log('All post delete');
            });
            return true;
        },
    },
};
