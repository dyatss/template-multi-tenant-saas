import { tenantCreate } from "../../../helpers/items/tenant/tenant.create";



// Provide resolver functions for the GraphQL schema
export const TenantResolver = {
  /**
   * A GraphQL Query for posts that uses a Post model to query MongoDB
   * and get all Post documents.
   */
  Query: {
    
  },
  /**
   * A GraphQL Mutation that provides functionality for adding post to
   * the posts list and return post after successfully adding to list
   */
  Mutation: {
    createTenant: async (parent, {TenantInput}) => {
      return await tenantCreate(TenantInput);
    }
  }
};
