import * as post from './post';
import * as devis from './devis';
import * as facture from './facture';
import * as product from './product';
import * as client from './client';
import * as employee from './employee';
import * as tenant from './tenant';
import * as enumeration from './enum';
import * as scalar from './scalar';

export const resolvers = [
    tenant.TenantResolver,
    post.postResolver,
    devis.devisResolver,
    facture.factureResolver,
    product.productResolver,
    employee.EmployeeResolver,
    client.clientResolver,
    /* Enum */
    enumeration.enumeration,
    /* Scalar */
    scalar.scalar,
];
