import { Post } from '../../models';
import connectDB from '../../../services/mongo.connect.js';
import { getModel } from '../../../helpers/Switchdb/model.find.js';
import { createPost } from '../../../helpers/items/post/post.create';
import { ApolloError } from 'apollo-server';
import { PubSub, withFilter } from 'graphql-subscriptions';
import { ObjectId, ObjectID } from 'bson';
import { context } from '../../midleware/auth';

const jwt = require('jsonwebtoken');

const pubSub = new PubSub();

// Provide resolver functions for the GraphQL schema
export const postResolver = {
    /**
     * A GraphQL Query for posts that uses a Post model to query MongoDB
     * and get all Post documents.
     */

    Query: {
        post: async (parent, _id, context) => {
            const PostModel = await getModel(
                [Post],
                context.companyName
            );
            console.log(ObjectId.isValid(_id));
            return PostModel.findById(_id);
        },

        posts: async (parent, _, context) => {
            const PostModel = await getModel(
                [Post],
                context.companyName
            );
            return PostModel.find({});
        },
    },
    /**
     * A GraphQL Mutation that provides functionality for adding post to
     * the posts list and return post after successfully adding to list
     */
    Mutation: {
        
        addPost: async (parent, post, context) => {
            // await context.auth
            const postCreate = await createPost(
                post,
                context
            );

            pubSub.publish('POST_CREATED', {
                PostCreated: {
                    _id: postCreate._id,
                    title: post.title,
                    content: post.content,
                    author: {
                        username: context.username,
                    },
                    tenant: context.companyName,
                },
            });

            return postCreate;
        },

        clearAllPost: async (parent, _, context) => {
            // await context.auth
            const PostModel = await getModel(
                [Post],
                context.companyName
            );
            PostModel.deleteMany({}, function (err) {
                console.log('All post delete');
            });

            pubSub.publish('POSTS_DELETE', {
                DeleteAllPosts: context.companyName,
            });
            return true;
        },
    },

    Subscription: {
        PostCreated: {
            subscribe: withFilter(
                () => pubSub.asyncIterator('POST_CREATED'),
                (payload, variable) => {
                    return (
                        payload.PostCreated.tenant ===
                        variable.tenant
                    );
                }
            ),
        },

        DeleteAllPosts: {
            subscribe: withFilter(
                () => pubSub.asyncIterator('POSTS_DELETE'),
                (payload, variable) => {
                    return (
                        payload.DeleteAllPosts ===
                        variable.tenant
                    );
                }
            ),
        },
    },
};
