import { Client } from '../../models';
import { getModel } from '../../../helpers/Switchdb/model.find.js';
import { AddClient } from '../../../helpers/items/client/client.create';
import { UpdateClient } from '../../../helpers/items/client/client.update';
import { DeleteClient, DeleteClients } from '../../../helpers/items/client/client.delete';



// Provide resolver functions for the GraphQL schema
export const clientResolver = {
    /**
     * A GraphQL Query for posts that uses a Post model to query MongoDB
     * and get all Post documents.
     */
    Query: {
        client: async (parent, { _id }, context) => {
            const clientModel = await getModel([Client], context.companyName);


            return clientModel.findById(_id)
        }
    },
    /**
     * A GraphQL Mutation that provides functionality for adding post to
     * the posts list and return post after successfully adding to list
     */
    Mutation: {
        createClient: async (parent, { ClientInput }, context) => {
            try {

                return await AddClient(ClientInput, context)

            } catch (error) {
                throw new Error(error);

            }
        },

        updateClient: async (
            parent,
            { ClientInput, id },
            context
        ) => {
            const ProductCreate = await UpdateClient(
                ClientInput,
                id,
                context
            );
            return ProductCreate;
        },

        deleteClient: async (parent, { id }, context) => {
            const ClientCreate = await DeleteClient(
                id,
                context
            );
            return ClientCreate;
        },

        deleteClients: async (
            parent,
            { ids },
            context
        ) => {
            const ClientCreate = await DeleteClients(
                ids,
                context
            );
            return ClientCreate;
        },
    },
}
