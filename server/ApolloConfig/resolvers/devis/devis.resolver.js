import { Devis, Product } from '../../models';
import connectDB from '../../../services/mongo.connect.js';
import { getModel } from '../../../helpers/Switchdb/model.find.js';
import { createPost } from '../../../helpers/items/post/post.create';
import { ApolloError } from 'apollo-server';
import { ObjectId, ObjectID } from 'bson';
import { context } from '../../midleware/auth';
import { AddDevis } from '../../../helpers/items/devis/devis.create';
import { UpdateDevis } from '../../../helpers/items/devis/devis.update';
import {
    DeleteDevis,
    DeleteAllDevis,
} from '../../../helpers/items/devis/devis.delete';
import mongoose from 'mongoose';
import { getDevis } from '../../../helpers/items/devis/devis.get';
const jwt = require('jsonwebtoken');

// Provide resolver functions for the GraphQL schema
export const devisResolver = {
    /**
     * A GraphQL Query for posts that uses a Post model to query MongoDB
     * and get all Post documents.
     */
    Query: {
        devis: async (parent, {_id}, context) => {
            const devis = getDevis(
                _id,
                context
            )
            return devis
        },

        allDevis: async (parent,_, context) => {
            const DevisModel = await getModel(
                [Devis],
                context.companyName
            );
            return DevisModel.find({});
        },

        devisByYear: async (parent, {year} , context) => {
            const DevisModel = await getModel(
                [Devis],
                context.companyName
            );
            return DevisModel.find({
                date_validation: {
                    $gte: new Date(year, 0, 0),
                    $lt: new Date(year + 1, 1, 1),
                },
            });
        },
    },
    /**
     * A GraphQL Mutation that provides functionality for adding post to
     * the posts list and return post after successfully adding to list
     */
    Mutation: {
        createDevis: async (
            parent,
            DevisInput,
            context
        ) => {
            const DevisCreate = await AddDevis(
                DevisInput.DevisInput,
                context
            );
            return DevisCreate;
        },

        updateDevis: async (
            parent,
            DevisInput,
            context
        ) => {
            const DevisCreate = await UpdateDevis(
                DevisInput.DevisInput,
                DevisInput.id,
                context
            );
            return DevisCreate;
        },

        deleteDevis: async (parent, { id }, context) => {
            const DevisCreate = await DeleteDevis(
                id,
                context
            );
            return DevisCreate;
        },

        deleteAllDevis: async (
            parent,
            { ids },
            context
        ) => {
            const DevisCreate = await DeleteAllDevis(
                ids,
                context
            );
            return DevisCreate;
        },

        clearAllDevis: async (parent, _, context) => {
            // await context.auth
            const DevisModel = await getModel(
                [Devis],
                context.companyName
            );
            DevisModel.deleteMany({}, function (err) {
                console.log('All Devis delete');
            });
            return true;
        },
    },
};
