export const enumeration = {
    /**
     * A GraphQL Query for posts that uses a Post model to query MongoDB
     * and get all Post documents.
     */
    Role: {
        Super_Admin: 'Super Admin',
        Admin: 'Admin',
        Access: 'Access',
    },

    Status: {
        brouillon: 'Brouillon',
        valider: 'Valider',
        regler: 'Regler',
        partiellement: 'Partiellement',
        annuler: 'Annuler',
        payer: 'Payer',
    },

};
