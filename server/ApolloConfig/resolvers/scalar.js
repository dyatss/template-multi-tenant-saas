// Provide resolver functions for the GraphQL schema
import { Kind } from 'graphql/language'

const EMAIL_REGEX = new RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
)


export const scalar = {
    /**
     * A GraphQL Query for posts that uses a Post model to query MongoDB
     * and get all Post documents.
     */

    EmailAddress: {
        description:"L'adresse email ou adresse de messagerie électronique est une chaîne de caractère permettant l'acheminement d'un message électronique sur le réseau Internet. Elle se compose d'un identifiant utilisateur et d'un nom de domaine avec extension qui sont séparés par un arobase (@).",
        __parseValue(value) {
            if (typeof value !== 'string') {
                throw new TypeError('Value is not string')
            }

            if (!EMAIL_REGEX.test(value)) {
                throw new TypeError(`Value is not a valid email address: ${value}`)
            }

            return value
        },
        __serialize(value) {
            return value
        },
        __parseLiteral(ast) {
            if (ast.kind !== Kind.STRING) {
                throw new Error(
                    `Value is not string : ${ast.kind}`
                )
            }

            if (!EMAIL_REGEX.test(ast.value)) {
                throw new Error(`Value is not a valid email address: ${ast.value}`)
            }

            return ast.value
        }
    }

};