import { Product } from '../../models';
import { getModel } from '../../../helpers/Switchdb/model.find.js';
import { AddProduct } from '../../../helpers/items/product/product.create';
import { UpdateProduct } from '../../../helpers/items/product/product.update';
import { DeleteProduct, DeleteProducts } from '../../../helpers/items/product/product.delete';



// Provide resolver functions for the GraphQL schema
export const productResolver = {
    /**
     * A GraphQL Query for posts that uses a Post model to query MongoDB
     * and get all Post documents.
     */
    Query: {
        product: async (parent, { _id }, context) => {
            const productModel = await getModel([Product], context.companyName)


            return productModel.findById(_id)
        },

        products: async (parent, _, context) => {
            const productModel = await getModel([Product], context.companyName)


            return productModel.find({})
        },
    },
    /**
     * A GraphQL Mutation that provides functionality for adding post to
     * the posts list and return post after successfully adding to list
     */
    Mutation: {
        createProduct: async (
            parent,
            { ProductInput },
            context
        ) => {
            const ProductCreate = await AddProduct(
                ProductInput,
                context
            );
            return ProductCreate;
        },

        updateProduct: async (
            parent,
            { ProductInput, id },
            context
        ) => {
            const ProductCreate = await UpdateProduct(
                ProductInput,
                id,
                context
            );
            return ProductCreate;
        },

        deleteProduct: async (parent, { id }, context) => {
            const FactureCreate = await DeleteProduct(
                id,
                context
            );
            return FactureCreate;
        },

        deleteProducts: async (
            parent,
            { ids },
            context
        ) => {
            const FactureCreate = await DeleteProducts(
                ids,
                context
            );
            return FactureCreate;
        },
    },
};
