import dotenv from 'dotenv';

const result = dotenv.config({
    path: `./env/.env.${process.env.NODE_ENV}`,
});

console.log(`✔ Environement is ${process.env.NODE_ENV}`);

if (!result.parsed.NODE_ENV || !process.env.NODE_ENV) {
    throw Error('NODE_ENV is empty.');
}

if (result.error) {
    throw result.error;
}

module.exports = result;