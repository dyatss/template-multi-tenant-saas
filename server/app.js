import './lib/dotenv'; // Important ::: initier les variables d'environnement en premier.
import express from 'express';
import cors from 'cors';

import { ApolloServer } from 'apollo-server-express';
import { ApolloServerPluginDrainHttpServer } from 'apollo-server-core';

import { typeDefs } from './ApolloConfig/TypeDefs';
import { resolvers } from './ApolloConfig/resolvers';
import { context } from './ApolloConfig/midleware/auth';

import { createServer } from 'http';

import { makeExecutableSchema } from '@graphql-tools/schema';
import { WebSocketServer } from 'ws';
import { useServer } from 'graphql-ws/lib/use/ws';
import bodyParser from 'body-parser';

async function run() {
    // Create the schema, which will be used separately by ApolloServer and
    // the WebSocket server.
    const schema = makeExecutableSchema({
        typeDefs,
        resolvers,
    });

    const app = express();

    const httpServer = createServer(app);

    // Create our WebSocket server using the HTTP server we just set up.
    const wsServer = new WebSocketServer({
        server: httpServer,
        path: '/graphql',
    });

    // Save the returned server's info so we can shutdown this server later
    const serverCleanup = useServer({ schema }, wsServer);

    // Create Apollo server
    const server = new ApolloServer({
        schema,
        context,
        introspection: true,
        plugins: [
            // Proper shutdown for the HTTP server.
            ApolloServerPluginDrainHttpServer({
                httpServer,
            }),

            // Proper shutdown for the WebSocket server.
            {
                async serverWillStart() {
                    return {
                        async drainServer() {
                            await serverCleanup.dispose();
                        },
                    };
                },
            },
        ],
    });

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(cors());

    await server.start();

    // Use Express app as middleware in Apollo Server instance
    server.applyMiddleware({ app });

    // @ts-ignore
    require('./services/stripes')(app);

    // Listen server
    httpServer.listen({ port: 5000 }, () => {
        console.log(
            `🚀Server ready at http://localhost:5000${server.graphqlPath}`
        );
    });
}

run();
