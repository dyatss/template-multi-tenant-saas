import mongoose from 'mongoose';
import dotenv from 'dotenv';
dotenv.config();

const mongoOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    autoIndex: true,
    connectTimeoutMS: 10000,
    socketTimeoutMS: 30000,
};

function connectDB() {
    require('../ApolloConfig/models');
    return new Promise((resolve, reject) => {
        const mongoURL = `mongodb://mongo:mongo@localhost:27017/?authSource=admin`;
        mongoose
            .connect(mongoURL, mongoOptions)
            .then((conn) => {
                resolve(conn);
            })
            .catch((error) => reject(error));
    });
}

export default connectDB;
