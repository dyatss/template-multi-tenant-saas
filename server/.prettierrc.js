// prettier.config.js or .prettierrc.js
module.exports = {
    arrowParens: 'always',
    trailingComma: 'es5',
    printWidth: 60,
    tabWidth: 4,
    semi: true,
    singleQuote: true,
    bracketSpacing: true,
    jsxBracketSameLine: true,
    endOfLine: 'lf',
};
