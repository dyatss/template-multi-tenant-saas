import connectDB from '../services/mongo.connect.js'
import { schema, Tenant } from '../ApolloConfig/models/index.js'
import { Employee } from '../ApolloConfig/models/index.js'


const CompanySchemas = Employee
const TenantSchemas = Tenant

/** Switche to db on same connection pool
 * @return new connection
 */
 const switchDB = async (dbName, dbSchema) => {

    // let shema;
    // if(dbSchema === "tenant"){
    //     shema = TenantSchemas
    // } else if(dbSchema === "company") {
    //     shema = CompanySchemas
    // } else {
    //     shema = dbSchema
    // }
    const mongoose = await connectDB()
    if (mongoose.connection.readyState === 1) {
      
      const db = mongoose.connection.useDb(dbName, { useCache: false })
    //   Prevent from schema re-registration
      if (!Object.keys(db.models).length) {
        dbSchema.map((item) => {
          item.forEach((sch, modelName) => {
            db.model(modelName, sch)
          })
          
        })
      }
      return db
    }
    throw new Error('err')
  }

  const getDBModel = async (db, modelName) => {
      return db.model(modelName)
    }

  const getAllTenants = async () => {
    const tenantDB = await switchDB('AppTenants', TenantSchemas)
    const tenantModel = await getDBModel(tenantDB, 'tenant')
    const tenants = await tenantModel.find({})
    return tenants
  }


  export {switchDB, getDBModel, getAllTenants}; 