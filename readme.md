# Template Saas

1. Follow instructions on Docker Section to set up the environment.
2. Follow instructions on Server Section to set up the server.
3. Follow instructions on Front Section to set up the client.
4. Run dev script from root directory with npm to launch server and client at same time :

> npm install

> npm run dev

## Deploiement Dependencies

-   "nodemon": nodemon is a package that runs the node.js application and listen to any file change, updating the entire app.
-   "concurrently": Concurrently allows us to run multiple npm commands at the same time.
-   "onchange": Use glob patterns to watch file sets and run a command when anything is added, changed or deleted.
-   "prettier": Prettier is an opinionated code formatter.
-   "pretty-quick": Runs Prettier on your changed files.
-   "husky" : Husky can prevent bad git commit, git push and more...

## Docker

### Services

-   mongo : MongoDB Database

Change directory to Docker Folder and run docker-compose :

> cd docker

> docker-compose up -d

-   LDAP : (only for development)

Change directory to Docker Folder and run docker-compose :

> cd docker.dev

> docker-compose up -d

## Server

### Services

Run dev script from root directory with npm to launch server :

> npm run start-dev (for development configuration)

> npm run start (for production configuration)

### Dependencies

-   "express": a very popular framework for Node.js applications.
-   "body-parser": is used to parse incoming request bodies in a middleware before your handlers, available under the req.body property.
-   "mongoose": is a MongoDB object modeling tool designed to work in an asynchronous environment.
-   "graphql": is a query language for APIs.
-   "apollo-server-express": is the Express and Connect integration of GraphQL Server.

### Server Dev Dependencies

-   "nodemon": nodemon is a package that runs the node.js application and listen to any file change, updating the entire app.
-   "concurrently": Concurrently allows us to run multiple npm commands at the same time.
-   "onchange": Use glob patterns to watch file sets and run a command when anything is added, changed or deleted.
-   "prettier": Prettier is an opinionated code formatter.
-   "pretty-quick": Runs Prettier on your changed files.
-   "husky" : Husky can prevent bad git commit, git push and more...

### Server Instructions

Make sure to change directory to server and run npm i to install dependencies from package.json

> npm install

## Client

### Services

Run dev script from root directory with npm to launch client :

> npm run start (for development configuration)

> npm run build (for production configuration)

### Client Dependencies

-   "http-proxy-middleware": is used to create a proxy from our react app to the backend app while on development.
-   "axios": is a very popular promise based HTTP client for the browser and node.js.

### Client Instructions

Make sure to change directory to client and run npm i to install dependencies from package.json

> npm install
