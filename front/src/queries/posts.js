import { gql } from "graphql-tag";

/* ************************************************************************* */
/* ********************************  QUERIES   ***************************** */
/* ************************************************************************* */

export const GET_POSTS = gql`
  query Query {
    posts {
      _id
      title
      content
      author {
        username
      }
    }
  }
`;

/* ************************************************************************* */
/* ******************************  MUTATIONS   ***************************** */
/* ************************************************************************* */

export const ADD_POST = gql`
  mutation AddPost($title: String!, $content: String!) {
    addPost(title: $title, content: $content) {
      title
      content
      author {
        username
      }
    }
  }
`;

export const DELETE_ALL_POST = gql`
  mutation Mutation {
    clearAllPost
  }
`;

/* ************************************************************************* */
/* ***************************  SUBSCRIPTIONS   **************************** */
/* ************************************************************************* */

export const POST_SUBSCRIPTION = gql`
  subscription PostCreated($tenant: String) {
    PostCreated(tenant: $tenant) {
      _id
      title
      content
      author {
        username
      }
    }
  }
`;

export const POST_DELETE_SUBSCRIPTION = gql`
  subscription DeleteAllPosts($tenant: String) {
    DeleteAllPosts(tenant: $tenant)
  }
`;
