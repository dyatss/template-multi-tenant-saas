import { gql } from "graphql-tag";

/* ************************************************************************* */
/* ********************************  QUERIES   ***************************** */
/* ************************************************************************* */

/* ************************************************************************* */
/* ******************************  MUTATIONS   ***************************** */
/* ************************************************************************* */

export const REGISTER_USER = gql`
  mutation RegisterUser($registerInput: RegisterInput, $locataire: String) {
    registerUser(RegisterInput: $registerInput, locataire: $locataire) {
      email
      username
      token
    }
  }
`;

export const LOGIN_USER = gql`
  mutation LoginUser($loginInput: LoginInput, $locataire: String) {
    loginUser(LoginInput: $loginInput, locataire: $locataire) {
      email
      username
      token
    }
  }
`;

/* ************************************************************************* */
/* ***************************  SUBSCRIPTIONS   **************************** */
/* ************************************************************************* */
