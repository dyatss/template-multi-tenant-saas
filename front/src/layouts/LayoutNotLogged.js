import React, { useContext } from "react";
import { Outlet } from "react-router-dom";
import { Navbar } from "../components/navbar/Navbar";
import { AuthContext } from "../context/authContext";
import { useIfLogged } from "../utility/redirectUser";

export const LayoutNotLogged = ({ children }) => {
  const context = useContext(AuthContext);
  useIfLogged(context.user);

  return (
    <div>
      <Navbar />
      <Outlet />
    </div>
  );
};
