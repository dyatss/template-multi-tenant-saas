import React, { useContext } from "react";
import { Outlet } from "react-router-dom";
import { Navbar2 } from "../components/navbar/Navbar2";
import NavbarLogged from "../components/navbar/NavbarLogged";
import { AuthContext } from "../context/authContext";
import { useIfNotLogged } from "../utility/redirectUser";

export const LayoutLogged = ({ children }) => {
  const { user } = useContext(AuthContext);

  useIfNotLogged(user);

  return (
    <>
      <NavbarLogged />
      <Outlet />
    </>
  );
};
