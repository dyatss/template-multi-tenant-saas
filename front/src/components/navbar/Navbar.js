import React, { useContext } from 'react'
import { AppBar, Box, Toolbar, Typography, Button } from '@mui/material'
import { Link, Navigate, useNavigate } from "react-router-dom"
import { AuthContext } from '../../context/authContext';


export const Navbar = () => {
    let navigate = useNavigate();
    const { user, logout } = useContext(AuthContext);

    const onLogout = () => {
        logout();
        navigate('/login')
    }
    

    console.log(user)
  return (
    <Box sx={{flexGrow: 1}}>
        <AppBar position='static'>
            <Toolbar>
                <Typography variant='h5' component="div">
                    <Link to="/" >Logo</Link>
                </Typography>
                <Box alignItems="right" sx={{flexGrow: 1, textAlign:"right"}}>
                    { user?(
                        <Button style={{textDecoration: "none", color:"white"}} onClick={onLogout}>Logout</Button>
                    )
                    :
                    (
                        <>
                            <Link to="/login" style={{marginRight: "10px"}} >Login</Link>
                            <Link to="/register" >Register</Link>
                        </>
                    )
                    }
                    
                </Box>
            </Toolbar>
        </AppBar>
    </Box>
  )
}
