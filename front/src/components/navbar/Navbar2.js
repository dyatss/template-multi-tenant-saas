import React from "react";
import { AppBar, Box, Toolbar, Typography, Button } from "@mui/material";
import { Link } from "react-router-dom";

export const Navbar2 = () => {
  return (
    <Box sx={{ flexGrow: 1, background: "#000" }}>
      <Toolbar>
        <Box alignItems="center" sx={{ flexGrow: 1, textAlign: "center" }}>
          <Link to="/dashboard" style={{ marginRight: "10px" }}>
            Facture
          </Link>
          <Link to="/dashboard">Devis</Link>
        </Box>
      </Toolbar>
    </Box>
  );
};
