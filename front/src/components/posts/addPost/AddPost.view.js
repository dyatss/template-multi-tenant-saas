import React from "react";

// mui
import { TextField, Button, Stack, Alert, Grid } from "@mui/material";
import { AddPost } from "./AddPost";

export const AddPostView = () => {
  const { onChange, onSubmit, values, errors } = AddPost();

  return (
    <Grid item xs={6}>
      <form onSubmit={onSubmit}>
        <Stack spacing={2} paddingBottom={2}>
          <TextField
            label="Title"
            name="title"
            onChange={onChange}
            value={values.title}
          />
          <TextField
            label="content"
            name="content"
            multiline
            rows={6}
            onChange={onChange}
            value={values.content}
          />
          {errors.map((error) => (
            <Alert severity="error">{error.message}</Alert>
          ))}
          <Button type="submit" variant="contained">
            Post
          </Button>
        </Stack>
      </form>
    </Grid>
  );
};
