import React, { useState } from "react";
import { useForm } from "../../../utility/hooks";
import { WithMutationAddPost } from "../../../utility/graphql";

export const AddPost = () => {
  const [errors, setErrors] = useState([]);

  function addPostCallback() {
    setErrors([]);
    postUser();
    reset();
  }

  const { onChange, onSubmit, values, reset } = useForm(addPostCallback, {
    title: "",
    content: "",
  });

  const { postUser } = WithMutationAddPost(values, setErrors);

  return {
    onChange,
    onSubmit,
    values,
    errors,
  };
};
