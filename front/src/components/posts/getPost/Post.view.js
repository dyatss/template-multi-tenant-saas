import React from "react";
import { Posts } from "./Posts";

// mui

import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { Button, CardActionArea } from "@mui/material";
import { Grid } from "@mui/material";
import { CardsBone } from "../../../skeletons/cards.bone";

export const PostsView = () => {
  const { loading, allPosts, PostDelete } = Posts();
  return (
    <>
      <Grid item xs={6} justifyContent="center">
        <Button onClick={PostDelete}>Clear all post</Button>
        {loading ? (
          <CardsBone />
        ) : (
          allPosts?.map(({ _id, title, content, author }) => (
            <Card sx={{ maxWidth: 500, marginBottom: 2 }} key={_id}>
              <CardActionArea>
                <CardContent>
                  <Typography gutterBottom variant="h5" component="div">
                    {title}
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    {content}
                  </Typography>
                  <Typography
                    variant="body2"
                    color="text.secondary"
                    textAlign="end"
                  >
                    {author.username}
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          ))
        )}
      </Grid>
    </>
  );
};
