import React, { useState, useEffect, useContext } from "react";

import {
  GET_POSTS,
  POST_DELETE_SUBSCRIPTION,
  POST_SUBSCRIPTION,
} from "../../../queries/posts";

import { useQuery, useSubscription } from "@apollo/react-hooks";
import { WithQueryGetPosts } from "../../../utility/graphql/posts/withQueryPost";
import { WithMutationDeleteAllPost } from "../../../utility/graphql/posts/withMutationPost";
import {
  WithSubscriptionDeleteAllPost,
  WithSubscriptionNewPost,
} from "../../../utility/graphql/posts/withSubscriptionPost";
import { AuthContext } from "../../../context/authContext";

export const Posts = () => {
  const [allPosts, setAllPosts] = useState([]);
  let subdomain = window.location.host.split(".")[0];

  const { loading, error } = WithQueryGetPosts(setAllPosts);

  const { PostDelete } = WithMutationDeleteAllPost();

  WithSubscriptionNewPost(allPosts, setAllPosts);

  WithSubscriptionDeleteAllPost(setAllPosts);

  if (loading) return <p>Loading ...</p>;

  return {
    loading,
    allPosts,
    PostDelete,
  };
};
