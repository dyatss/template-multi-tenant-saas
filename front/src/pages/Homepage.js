import React, { useContext } from "react";
import { AuthContext } from "../context/authContext";
import { useIfNotLogged } from "../utility/redirectUser";

import {
  TextField,
  Button,
  Container,
  Stack,
  Alert,
  Grid,
} from "@mui/material";

import { AddPostView } from "../components/posts/addPost/AddPost.view";
import { PostsView } from "../components/posts/getPost/Post.view";
import { Outlet } from "react-router-dom";

export const Homepage = () => {
  return (
    <>
      <h1>This is the homepage</h1>
      <Container spacing={2} maxWidth="xl">
        <Grid container spacing={12} justifyContent="center">
          <AddPostView />
          <PostsView />
        </Grid>
      </Container>
      <Outlet />
    </>
  );
};
