import React, { useContext, useState } from "react";
import { AuthContext } from "../context/authContext";
import { useForm } from "../utility/hooks";
import { useMutation } from "@apollo/react-hooks";

import { useNavigate } from "react-router-dom";

// material ui

import { TextField, Button, Container, Stack, Alert } from "@mui/material";

import { LOGIN_USER } from "../queries/employees";
import { useIfLogged } from "../utility/redirectUser";

export const Login = () => {
  const context = useContext(AuthContext);
  let navigate = useNavigate();
  const [errors, setErrors] = useState([]);
  let subdomain = window.location.host.split(".")[0];

  function loginUserCallback() {
    console.log("Callback hit");
    loginUser();
  }

  const { onChange, onSubmit, values } = useForm(loginUserCallback, {
    email: "",
    password: "",
  });

  const [loginUser, { loading }] = useMutation(LOGIN_USER, {
    update(proxy, { data: { loginUser: userData } }) {
      context.login(userData);
      navigate("/dashboard");
    },
    onError({ graphQLErrors }) {
      console.log(subdomain);
      setErrors(graphQLErrors);
    },
    variables: {
      loginInput: values,
      locataire: subdomain === "localhost:3000" ? "babelweb" : subdomain,
    },
  });

  return (
    <form onSubmit={onSubmit}>
      <Container spacing={2} maxWidth="sm">
        <h3>Login</h3>
        <p>This is the Login page, Login below to create an account!</p>
        <Stack spacing={2} paddingBottom={2}>
          <TextField label="Email" name="email" onChange={onChange} />
          <TextField
            type="password"
            label="Password"
            name="password"
            onChange={onChange}
          />
          {errors.map((error) => (
            <Alert severity="error">{error.message}</Alert>
          ))}
          <Button type="submit" variant="contained">
            Login
          </Button>
        </Stack>
      </Container>
    </form>
  );
};
