import React, { useContext, useState } from "react";
import { AuthContext } from "../context/authContext";
import { useForm } from "../utility/hooks";
import { useMutation } from "@apollo/react-hooks";

import { useNavigate } from "react-router-dom";

// material ui

import { TextField, Button, Container, Stack, Alert } from "@mui/material";

import { REGISTER_USER } from "../queries/employees";
import { useIfLogged } from "../utility/redirectUser";

export const Register = () => {
  const context = useContext(AuthContext);
  let navigate = useNavigate();
  const [errors, setErrors] = useState([]);
  let subdomain = window.location.host.split(".")[0];

  function registerUserCallback() {
    console.log("Callback hit");
    console.log("subdomaine : ", subdomain);
    registerUser();
  }

  const { onChange, onSubmit, values } = useForm(registerUserCallback, {
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
  });

  const [registerUser, { loading }] = useMutation(REGISTER_USER, {
    update(proxy, { data: { registerUser: userData } }) {
      context.login(userData);
      navigate("/");
    },
    onError({ graphQLErrors }) {
      setErrors(graphQLErrors);
    },
    variables: {
      registerInput: values,
      locataire: subdomain === "localhost:3000" ? "babelweb" : subdomain,
    },
  });

  useIfLogged(context.user);

  return (
    <form onSubmit={onSubmit}>
      <Container spacing={2} maxWidth="sm">
        <h3>Register</h3>
        <p>This is the register page, register below to create an account!</p>
        <Stack spacing={2} paddingBottom={2}>
          <TextField label="Username" name="username" onChange={onChange} />
          <TextField label="Email" name="email" onChange={onChange} />
          <TextField
            type="password"
            label="Password"
            name="password"
            onChange={onChange}
          />
          <TextField
            type="password"
            label="Confirm Password"
            name="confirmPassword"
            onChange={onChange}
          />
          {errors.map((error) => (
            <Alert severity="error">{error.message}</Alert>
          ))}
          <Button type="submit" variant="contained">
            Register
          </Button>
        </Stack>
      </Container>
    </form>
  );
};
