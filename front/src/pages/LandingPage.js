import { Avatar, Button, Grid, TextField } from "@mui/material";
import { Container, Stack } from "@mui/system";
import React from "react";
import { useForm } from "../utility/hooks";

// stripe

import { StripeContainer } from "../stripe/StripeContainer";
import { CheckoutForm } from "../stripe/CheckoutForm";

export const LandingPage = () => {
  function Callback() {
    console.log("Callback hit");
  }

  const { onChange, onSubmit, values } = useForm(Callback, {
    name: "",
    email: "",
    password: "",
    companyName: "",
  });

  return (
    <Grid container spacing={2}>
      <Grid item xs={6}>
        <h1 style={{ textAlign: "center" }}>Stripe Payements</h1>
        <form style={{ marginTop: "50px" }} onSubmit={onSubmit}>
          <Container spacing={2} maxWidth="sm">
            <Stack spacing={2} paddingBottom={2}>
              <TextField label="name" name="name" onChange={onChange} />
              <TextField
                type="email"
                label="email"
                name="email"
                onChange={onChange}
              />
              <TextField
                label="companyName"
                name="companyName"
                onChange={onChange}
              />
              <TextField
                type="password"
                label="Password"
                name="password"
                onChange={onChange}
              />
              <Button variant="contained">Next</Button>
            </Stack>
          </Container>
        </form>
      </Grid>
      <Grid item xs={6}>
        <StripeContainer />
      </Grid>
    </Grid>
  );
};
