import { useQuery } from "@apollo/client";
import { useState } from "react";
import { GET_POSTS } from "../../../queries/posts";




export const WithQueryGetPosts = (setAllPosts) => {


    const { loading, error, data } = useQuery(GET_POSTS,{
        onCompleted: (data) => {
            setAllPosts(data.posts)
        },
        fetchPolicy: "no-cache" 
    })

    return {
        loading,
        error,
    }
}