import { useSubscription } from "@apollo/client";
import { useContext, useState } from "react";
import { AuthContext } from "../../../context/authContext";

import {
  POST_DELETE_SUBSCRIPTION,
  POST_SUBSCRIPTION,
} from "../../../queries/posts";

export const WithSubscriptionNewPost = (allPosts, setAllPosts) => {
  const { user } = useContext(AuthContext);

  useSubscription(POST_SUBSCRIPTION, {
    onSubscriptionData: (data) => {
      console.log("enter sub");
      const newPost = data.subscriptionData.data.PostCreated;
      setAllPosts((posts) => [...allPosts, newPost]);
    },
    variables: { tenant: user?.companyName },
  });
};

export const WithSubscriptionDeleteAllPost = (setAllPosts) => {
  const { user } = useContext(AuthContext);

  useSubscription(POST_DELETE_SUBSCRIPTION, {
    onSubscriptionData: (data) => {
      const deletes = data.subscriptionData.data.DeleteAllPosts;

      if (deletes) {
        setAllPosts([]);
      }
    },
    variables: { tenant: user?.companyName },
  });
};
