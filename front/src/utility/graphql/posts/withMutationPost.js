import { useMutation } from "@apollo/client";
import { useState } from "react";
import { ADD_POST, DELETE_ALL_POST, GET_POSTS } from "../../../queries/posts";

export const WithMutationAddPost = (values, setErrors = {}) => {
  const [postUser, { loading }] = useMutation(ADD_POST, {
    onError({ graphQLErrors }) {
      setErrors(graphQLErrors);
    },
    variables: { title: values.title, content: values.content },
  });

  return {
    postUser,
    loading,
  };
};

export const WithMutationDeleteAllPost = () => {
  const [PostDelete, { loading }] = useMutation(DELETE_ALL_POST, {
    onError({ graphQLErrors }) {
      console.log(graphQLErrors);
    },
  });

  return {
    PostDelete,
    loading,
  };
};
