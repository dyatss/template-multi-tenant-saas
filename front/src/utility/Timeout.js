import React, { useState, useEffect } from 'react'

export const Timeout = (props) => {
    const { children, ms } = props
    const [loading, setLoading] = useState(false)


    useEffect(() => {
        const load = setTimeout(() => {
            setLoading(true)
        }, ms);

        return () => {
            clearTimeout(load)
        }
    }, [loading])



    return loading?(
        { children }
    ):null
}
