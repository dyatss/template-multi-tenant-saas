import { useState } from 'react'

export const useForm = (callback, initialState = {}) => {
    // Password
    // Email
    // {"password": "", "email" : ""}
    
    const [values, setValues] = useState(initialState);
    const [defaultValue, setDefaultValue] = useState(initialState);

    const onChange = (event) => {
        setValues({ ...values, [event.target.name] : event.target.value });
    };

    const onSubmit = (event) => {
        event.preventDefault();
        callback();
    }

    const reset = (event) => {
        setValues(defaultValue)
    }

    return {
        onChange,
        onSubmit,
        values,
        reset
    }
}