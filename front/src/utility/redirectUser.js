import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

export const useIfLogged = (user) => {
  let navigate = useNavigate();
  let subdomain = window.location.host.split(".")[0];

  useEffect(() => {
    console.log("subdomaine : ", subdomain);
    if (subdomain === "localhost:3000") {
      navigate("/");
    }
    if (user) {
      navigate("/dashboard");
    }
  }, [navigate]);
};

export const useIfNotLogged = (user) => {
  let navigate = useNavigate();

  useEffect(() => {
    if (!user) {
      navigate("/login");
    }
  }, []);
};
