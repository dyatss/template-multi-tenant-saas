import logo from "./logo.svg";
import "./App.css";
import {
  Routes,
  Route,
  useRoutes,
  Navigate,
  useParams,
  Outlet,
} from "react-router-dom";
import { Homepage, Login, Register, LandingPage } from "./pages";
import { Navbar } from "./components/navbar/Navbar";
import { LayoutNotLogged } from "./layouts/LayoutNotLogged";
import { routes } from "./routes/routes";
import { LayoutLogged } from "./layouts/LayoutLogged";
import { Home } from "@mui/icons-material";
import { PageNotFound } from "./pages/PageNotFound";

function App() {
  let subdomain = window.location.host.split(".")[0];
  console.log("subdomaine : ", subdomain)

  return (
    <Routes>
      {subdomain === "localhost:3000" ? (
        /* LandingPage route */
        <Route path="/">
          <Route path="" element={<LandingPage />} />
          <Route path="*" element={<Navigate to="/" replace />} />
        </Route>
      ) : (
        /* Not Logged routes / subdomaine*/
        <Route path="/" element={<LayoutNotLogged />}>
          <Route path="" element={<Navigate to="/login" replace />} />
          <Route path="login" element={<Login />} />
          <Route path="register" element={<Register />} />
          <Route path="404" element={<PageNotFound />} />
        </Route>
      )}

      {/* Logged routes */}
      <Route path="dashboard" element={<LayoutLogged />}>
        <Route path="" element={<Homepage />} />
      </Route>

      <Route path="*" element={<Navigate to="/login" replace />} />
    </Routes>
  );
}

export default App;
