import { Homepage, Login, Register } from "../pages";

export const routes = [
  {
    name: "homepage",
    path: "/",
    element: <Homepage />,
  },
  {
    name: "homepage",
    path: "/login",
    element: <Login />,
  },
  {
    name: "homepage",
    path: "/register",
    element: <Register />,
  },
];
