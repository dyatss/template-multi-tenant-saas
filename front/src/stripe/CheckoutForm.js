import React from "react";
import {
  CardElement,
  useStripe,
  useElements,
  PaymentElement,
} from "@stripe/react-stripe-js";
import axios from "axios";
import { elementAcceptingRef } from "@mui/utils";
import { Container, Stack } from "@mui/system";
import { Button } from "@mui/material";

export const CheckoutForm = () => {
  const stripe = useStripe();
  const elements = useElements();

  const onSubmit = async (event) => {
    event.preventDefault();
    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: "card",
      card: elements.getElement(CardElement),
    });
    if (!error) {
      console.log("Token Généré: ", paymentMethod);
      //envoi du token au backend
      try {
        const { id } = paymentMethod;
        const response = await axios.get(
          "http://localhost:5000/stripe/charge",
          {
            amount: 100,
            id: id,
          }
        );
        if (response.data.success) console.log("Payement réussi");
      } catch (error) {
        console.log("erreur!", error);
      }
    } else {
      console.log(error.message);
    }
  };

  return (
    <form onSubmit={onSubmit}>
      <Container spacing={2} maxWidth="sm" sx={{ marginTop: 20 }}>
        <Stack spacing={2} paddingBottom={2}>
          <CardElement
            options={{
              hidePostalCode: true,
            }}
          />
          <Button type="submit" variant="contained">
            Payer
          </Button>
        </Stack>
      </Container>
    </form>
  );
};
